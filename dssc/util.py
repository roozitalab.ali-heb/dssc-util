"""
    dssc.util package
"""
import logging
from logging import StreamHandler
from datadog import initialize as dd_init, api as dd_api

DD_TITLE='DSSC'
DD_TAGS=['DSSC', 'env:dev'], 
DD_LOG_FMT='[%(asctime)s][%(levelname)s][%(funcName)s:%(lineno)d] %(message)s'

class DDHandler(StreamHandler):
    """
        Datadog handler used to send log messages to datadog
    """

    def __init__(self, app_key, api_key, tags=None, title="DSSC"):
        """
            
        """
        StreamHandler.__init__(self)
        self.app_key = app_key
        self.api_key = api_key 
        self.tags = tags
        self.title=title
        dd_init(api_key=self.api_key, app_key=self.app_key)

    def data_dog_send(self,title,text,tags=[]):
        dd_api.Event.create(title=title, text=text, tags=tags)

    def emit(self, record):
        msg = self.format(record)
        try:
            self.data_dog_send(self.title,str(msg),tags=self.tags)
        except Exception as e:
            raise e


def get_dd_logger(api_key, app_key, title=DD_TITLE, tags=DD_TAGS, level=logging.DEBUG, fmt_str=DD_LOG_FMT):
    logging.basicConfig()
    dd_handler = DDHandler(app_key, api_key, tags=tags, title=title)
    formatter = logging.Formatter(fmt_str)
    dd_handler.setFormatter(formatter)
    dd_logger=logging.getLogger('dd_logger')
    dd_logger.setLevel(level)
    dd_logger.addHandler(dd_handler)
    return dd_logger

