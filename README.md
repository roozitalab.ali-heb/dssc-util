dssc-util -- DSSC util python package
=====================================

[![Current version on GITLAB](https://gitlab.com/roozitalab.ali-heb/dssc-util)]



Motivation
----------

A utility package to be used anywhere we use
python code.  

-  Sending events to datadog
-  TBD
    

Build Wheel
-----------

From within the package directory:

```console
$ python setup.py build bdist_wheel 

```

Installation
------------

To install dssc-util, simply:

```console
$ pip install dist/dssc_util-0.0.1-py3-none-any.whl
```


Usage
-----


Example:

```python

from dssc.util import get_dd_logger

title ='<EVENT-TITLE-HERE>'
tags=['DSSC', 'env:dev', 'test:example'],
api_key = '<DATADOG-API-KEY>',
app_key = '<DATADOG-APP-KEY>'
dd_log= get_dd_logger(api_key, app_key, title=title, tags=tags)
for i in range(5):
    dd_log.debug("TEST TEST TEST index{}".format(i))

```

