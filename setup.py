import codecs
import os
import re

from setuptools import setup, find_packages


PKG_NAME="dssc-util" 
VERSION='0.0.1'

with open("README.md", "r") as fh:
    long_description = fh.read()


if __name__ == "__main__":
    setup(
    name=PKG_NAME,
    version=VERSION,
    description="DSSC utils package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    python_requires='>=3.7',
    install_requires=[
       "datadog>=0.38.0",
       "requests>=2.23.0"
    ]
)
#    setup(
#        name=NAME,
#        description=find_meta("description"),
#        license=find_meta("license"),
#        url=find_meta("uri"),
#        version=find_meta("version"),
#        author=find_meta("author"),
#        author_email=find_meta("email"),
#        maintainer=find_meta("author"),
#        maintainer_email=find_meta("email"),
#        keywords=KEYWORDS,
#        long_description=read("README.rst"),
#        long_description_content_type="text/x-rst",
#        packages=PACKAGES,
#        package_dir={"": "src"},
#        options={"bdist_wheel": {"universal": "1"}},
#    )

